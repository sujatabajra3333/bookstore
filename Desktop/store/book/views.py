from django.shortcuts import render,get_object_or_404
from .models import  Category,Book
from django.db.models import Q
# Create your views here.
def book_list(request,category_slug=None):
    category = None
    categories = Category.objects.all()
    book = Book.objects.all()
    if request.method == "POST":
        search = request.POST.get('search')
        results = Book.objects.filter(Q(title__icontains=search)| Q(author__icontains=search))
        context={
            'result':results
        }
        return render(request,"book_list.html",context)
    if category_slug:
        category = get_object_or_404(Category,slug=category_slug)
        book = book.filter(category=category)
    return render(request, 'book_list.html', {'categories':categories,
                                              'category':category,
                                              'book':book,
                                              })

def book_detail(request,id):
    book=get_object_or_404(Book,id=id)
    return render(request,'book_detail.html',{'book':book})

