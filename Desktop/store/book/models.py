from django.db import models
from django.urls import  reverse


# Create your models here.



class Category(models.Model):
    name=models.CharField(max_length=150,db_index=True)
    slug=models.SlugField(unique=True)
    class Meta:
        ordering=('-name',)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('book:book_by_category', args=[self.slug])

class Book(models.Model):
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    title=models.CharField(max_length=200)
    desc=models.TextField(db_index=True)
    publish=models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    date_added = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    publisher = models.CharField(max_length=500)
    author = models.CharField(max_length=500)

    class Meta:
        ordering=('-title',)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
       return reverse('book:book_detail',args=[self.id,])
